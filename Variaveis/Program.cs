﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Variaveis
{
    class Program
    {
        static void Main(string[] args)
        {
            int idade = 21;
            string nome = "Rafael";
            bool bonitao = true;
            double salario = 850.50;

            Console.WriteLine("Idade: " + idade);
            Console.WriteLine("Nome: " + nome);
            Console.WriteLine("Bonitão: " + bonitao);
            Console.WriteLine("Salário: " + salario);

            Console.ReadKey();
        }
    }
}
